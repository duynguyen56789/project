import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

import Header from '../components/layouts/Header';
import Video from '../components/layouts/Video';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  video = {
    nameVideo: 'Ten video demo rat rat rat rat rat rat rat rat la dai',
    user: 'Ten demo',
    seeCount: '4 N',
    time: '4 tháng'
  };
  render() {
    return (
      <View style={{ flex: 1 }} >
        <Header userName='H' />
        <ScrollView>
          <Video video={this.video} />
          <Video video={this.video} />
          <Video video={this.video} />
          <Video video={this.video} />
          <Video video={this.video} />
          <Video video={this.video} />
          <Video video={this.video} />
        </ScrollView>
      </View>
    );
  }
}
